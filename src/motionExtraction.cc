/*
 * test.cc
 *
 *  Created on: Apr 11, 2015
 *      Author: iyerro
 */

#include "motionExtraction.hpp"

int main (int argc, char *argv[]) {

	std::stringstream ss;

	std::vector<std::string> videoIDList = getVideoIDList();

	for (unsigned int i=0; i<videoIDList.size(); i++) {
		std::cout << "Processing Video " << i + 1 << std::endl << std::endl;
		std::stringstream ss;
		ss << VIDEO_INPUT_PATH << videoIDList[i] << "/" << videoIDList[i] << "_rsrtdense_new.avi";
		motionExtraction *m = new motionExtraction(ss.str());
		m->processVideo();
		free(m);
	}

	return 0;
}

motionExtraction::motionExtraction(std::string videoFile) {
	std::cout<<"test";
	videoFilePath = videoFile;

	char *v = new char[videoFilePath.length() + 1];
	strcpy(v, videoFilePath.c_str());
	videoID = remove_extension(basename(v));
	std::string s = "_rsrtdense_new";
	std::string::size_type i = videoID.find(s);
	if (i != std::string::npos)
		videoID.erase(i, s.length());
	delete [] v;

	std::stringstream ss;

	ss.str(std::string());
	ss << "mkdir -p " << OUTPUT_PATH << "MOG/" << videoID << "/" << videoID << "_rsrtdense_new";
	system(ss.str().c_str());

	ss.str(std::string());
	ss << "mkdir -p " << OUTPUT_PATH << "MOG2/" << videoID << "/" << videoID << "_rsrtdense_new";
	system(ss.str().c_str());

}

void motionExtraction::processVideo () {
	CvCapture* capture;
	cv::Mat frame;
	int frameCount = 0;

	capture = cvCaptureFromFile(videoFilePath.c_str());

	cv::BackgroundSubtractorMOG *bgMOG = new cv::BackgroundSubtractorMOG(100, 5, 0.3);
	cv::BackgroundSubtractorMOG2 *bgMOG2 = new cv::BackgroundSubtractorMOG2(100, 10, true);

	if (capture) {
		while (true) {
			frame = cvQueryFrame(capture);
			if (!frame.empty()) {
				frameCount++;
				std::stringstream MOGOUT;
				MOGOUT << OUTPUT_PATH << "MOG/" << videoID << "/" << videoID << "_rsrtdense_new/" << frameCount-1 << ".png";

				std::stringstream MOG2OUT;
				MOG2OUT << OUTPUT_PATH << "MOG2/" << videoID << "/" << videoID << "_rsrtdense_new/" << frameCount-1 << ".png";

				cv::Mat foreMOG2, foreMOG;
				bgMOG2->operator()(frame,foreMOG2);
				bgMOG->operator()(frame,foreMOG);

				cv::threshold(foreMOG2, foreMOG2, 100, 255, cv::THRESH_BINARY);
				cv::threshold(foreMOG, foreMOG, 100, 255, cv::THRESH_BINARY);

				cv::imwrite(MOGOUT.str(), foreMOG);
				cv::imwrite(MOG2OUT.str(), foreMOG2);
			} else {
				std::cout << "Video " << videoID << " Processed" << std::endl;
				break;
			}

			int c = cv::waitKey(10);
			if ((char) c == 'c') {
				break;
			}
		}

		frame.release();
	}
	free(capture);
	return;
}

std::vector<std::string> getVideoIDList() {
	std::vector<std::string> videoIDList;

	std::string line;
	std::ifstream pittpatt(CSV_INPUT_PATH);

	if (!pittpatt.good()) {
		std::cout<<"Unable to Open PittPatt csv!" << std::endl;
	}

	std::getline(pittpatt, line, '\n');

	while (std::getline(pittpatt, line, '\n')) {
		pittpattface p;
		std::replace( line.begin(), line.end(), ',', '\t');
		std::istringstream iss (line);
		iss >> p.Video_id;
		iss >> p.Frame_id;
		iss.precision(6);
		iss.setf(std::ios::fixed);
		iss >> p.re_x;
		iss >> p.re_y;
		iss >> p.le_x;
		iss >> p.le_y;
		iss >> p.flt_x;
		iss >> p.flt_y;
		iss >> p.frb_x;
		iss >> p.frb_y;
		iss.unsetf(std::ios::fixed);

		if (!(std::find(videoIDList.begin(), videoIDList.end(), p.Video_id) != videoIDList.end())) {
			videoIDList.push_back(p.Video_id);
		}
	}

	pittpatt.close();

	return videoIDList;
}

std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(".");
    if (lastdot == std::string::npos) return filename;
    return filename.substr(0, lastdot);
}


