#include<opencv2/opencv.hpp>
#include<iostream>
#include<vector>

int main(int argc, char *argv[])
{
    cv::Mat frame;

    CvCapture* capture = cvCaptureFromFile("/s/parsons/h/proj/vision/data/cs510spring2015/assign02/05407d1144/05407d1144_rsrtdense_new.avi");

    cv::BackgroundSubtractorMOG *bgMOG = new cv::BackgroundSubtractorMOG(100, 5, 0.3);
    cv::BackgroundSubtractorMOG2 *bgMOG2 = new cv::BackgroundSubtractorMOG2(100, 10, true);

    cv::Mat kernel_ero = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(10,10));

    //cv::namedWindow("Frame");
    //cv::namedWindow("Background");

    for(;;)
    {
    	frame = cvQueryFrame(capture);
    	if (!frame.empty()) {
    		cv::Mat foreGMG;
    		cv::Mat foreMOG;
    		cv::Mat foreMOG2;
    		bgMOG2->operator()(frame,foreMOG2);
    		bgMOG->operator()(frame,foreMOG);
    		const std::string s("nmixtures");
    		std::cout << bgMOG2->getInt(s);
    		cv::threshold(foreMOG2, foreMOG2, 100, 255, cv::THRESH_BINARY);
			cv::morphologyEx(foreMOG2,foreMOG2,cv::MORPH_CLOSE,kernel_ero);

    		cv::threshold(foreMOG, foreMOG, 100, 255, cv::THRESH_BINARY);
			cv::morphologyEx(foreMOG,foreMOG,cv::MORPH_CLOSE,kernel_ero);

			//cv::morphologyEx(foreGMG,foreGMG,cv::MORPH_CLOSE,kernel_ero);
			/*std::vector<std::vector<cv::Point>> contours;
				cv::findContours( fore, contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE );
				cv::Mat mask = cv::Mat::zeros(fore.rows, fore.cols, CV_8UC1);
				cv::Rect maxArea;
				for (auto i : contours) {
					if (cv::boundingRect(i).area() > maxArea.area())
						maxArea = cv::boundingRect(i);
				}

				cv::rectangle(mask, maxArea, cv::Scalar(255, 255, 255), CV_FILLED);*/
			//frame.copyTo(masked,fore);
			//cv::imshow("Foregroundafter", fore);
			cv::imshow("Frame1",foreMOG2);
			//cv::imshow("Frame3",foreMOG);
    	}
    	else {
    		break;
    	}

    	int c = cv::waitKey(10);
		if ((char) c == 'c') {
			break;
		}
    }
    return 0;
}
