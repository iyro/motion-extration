/*
 * motionExtraction.hpp
 *
 *  Created on: Apr 11, 2015
 *      Author: iyerro
 */

#ifndef MOTIONEXTRACTION_HPP_
#define MOTIONEXTRACTION_HPP_


#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <libgen.h>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>

const std::string VIDEO_INPUT_PATH("/s/parsons/h/proj/vision/data/cs510spring2015/assign02/");
const std::string OUTPUT_PATH("/s/chopin/a/grad/iyerro/workspace/CS510_MotionExtraction/Output/");
const std::string CSV_INPUT_PATH("/s/chopin/a/grad/iyerro/CS510_Data/cs510assignment02detections_stab.csv");

struct pittpattface {
	std::string Video_id;
	int Frame_id;
	float flt_x;
	float flt_y;
	float frb_x;
	float frb_y;
	float re_x;
	float re_y;
	float le_x;
	float le_y;
};

struct configvalues {
	int minNeigh;
	float scale;
	int sizeval;
};

std::map <int, struct configvalues> configmap;

struct configOutputMetric {
	std::string Video_id;
	std::string Cascade_id;
	int configID;
	int configMinNeigh;
	float configScale;
	int configSizeVal;
	int Tot_Fls_Pos;
	int Tot_Pos;
	float Fls_Pos_Rt;
	int Tot_Fls_Neg;
	int Tot_Pot_Neg;
	float Fls_Neg_Rt;
};

std::vector<std::string> getVideoIDList();
std::string remove_extension(const std::string& filename);

class motionExtraction {
private:
	std::string videoFilePath;
	std::string videoID;

public:
	motionExtraction(std::string videoFile);
	void processVideo ();
};


#endif /* MOTIONEXTRACTION_HPP_ */
